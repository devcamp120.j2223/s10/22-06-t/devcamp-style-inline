import image from './assets/images/48.jpg'

const devcampAvatar = {
  borderRadius: "50%",
  marginTop: "-60px"
};

const style = {
  devcampContainer : {
    border: "1px solid #cacaca",
    width: "800px",
    margin: "0 auto",
    textAlign: "center",
    marginTop: "100px"
  },
  devcampQuote : {
    fontSize: "16px"
  },  
  devcampIntro : {
    fontSize: "13px"
  },  
  devcampName : {
    fontWeight: "bold",
    color: "blue"
  }
}

function App() {
  return (
    <div style = {style.devcampContainer}>
      <img src = {image} style = {devcampAvatar} />
      <p style = {{fontSize:"16px"}}>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p style={style.devcampIntro}>
        <span style={style.devcampName}>Tammy Stevens</span> - Front End Developer
      </p>
    </div>
  );
}

export default App;
